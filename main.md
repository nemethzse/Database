> #### _kódmágia_

# Adatbázis alapok

## Tananyag

- [Relációs adatbázisok](materials/definitions1.md)
- [SQL](materials/definitions2.md)
- [Kötelező olvasnivaló](http://www.inf.u-szeged.hu/~katona/db-ea1.pdf)

## Feladatok

- [1 .E-K modell - Nemzetközi táncverseny](tasks/task-1.md)
- [SOL E-K modell - Nemzetközi táncverseny](tasks/task-1-solution.md)
- [2. E-K modell - Betegnyilvantartó](tasks/task-2.md)
- [SOL E-K modell - Betegnyilvantartó](tasks/task-2-solution.md)
- [3. E-K modell - Online szakácskönyv](tasks/task-3.md)
- [SOL E-K modell - Online szakácskönyv](tasks/task-3-solution.md)
- [4. Relációséma - Nemzetközi táncverseny](tasks/task-4.md)
- [SOL Relációséma - Nemzetközi táncverseny](tasks/task-4-solution.md)
- [5. Relációséma - Betegnyilvantartó](tasks/task-5.md)
- [SOL Relációséma - Betegnyilvantartó](tasks/task-5-solution.md)
- [6. Relációséma - Online szakácskönyv](tasks/task-6.md)
- [SOL Relációséma - Online szakácskönyv](tasks/task-6-solution.md)
- [7. Adatbázis készítés - Nemzetközi táncverseny](tasks/task-7.md)
- [8. Adatbázis készítés - Betegnyilvantartó](tasks/task-8.md)
- [9. Adatbázis készítés - Online szakácskönyv](tasks/task-9.md)

> #### _kódmágia_
