> #### _kódmágia_

# Adatbázis készítés - Nemzetközi táncverseny

Vegyük az elkészített relációsémánkat:

```
TÁNC(tánc neve)
ZENE(cím, műfaj, előadó)
CSOPORT(nemzetiség, név, pontszám, átlag életkor)

TÁNCOL(nemzetiség, tánc neve, cím)
```

Hozzuk létre a MySQL adatbázist és vigyünk fel 7 demo tetszés szerinti demo adatot.
> #### _kódmágia_