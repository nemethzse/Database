> #### _kódmágia_

# Egyed-Kapcsolat diagramm - Betegnyilvantartó

Az   orvosok elvárásainak   megfelelően   az   adatbázisnak   tartalmaznia   kell   a   betegek   személyi   adatait,
gyógyszerérzékenységüket,   az   egyes   vizsgálatok   időpontjait   és   a   felírt   gyógyszereket.
Feltételezzük, hogy
- egy vizsgálaton több betegség is diagnosztizálható,
- egy betegségre több gyógyszer is felírható,
- egy-egy páciens több gyógyszerre is lehet érzékeny, 
- egy vizsgálatot a dátum és a vizsgálat kódja határoz meg egyértelműen. 

Megoldás:

![Megoldás](gifs/betegnyilvantarto.JPG)

> #### _kódmágia_