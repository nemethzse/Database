> #### _kódmágia_

# Relációséma- Betegnyilvantartó

Vegyük az elkészített E-K diagrammunkat:

![Megoldás](gifs/betegnyilvantarto.JPG)

Készítsük el a digram alapján a relációsémánkat!

Megoldás:
```
BETEG(taj, név, lakcím)
GYÓGYSZER(gyógyszer neve)
BETEGSÉG(betegség neve)
VIZSGÁLAT(vkód, dátum)

ÉRZÉKENY(taj, gyógyszer neve)
GYÓGYSZERT FELÍR(gyógyszer neve, betegség neve, vkód, dátum)
LÁTOGAT(taj, vkód, dátum)
DIAGNOSZTIZÁL(betegség neve, vkód, dátum)
```

> #### _kódmágia_