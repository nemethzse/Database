> #### _kódmágia_

# Egyed-Kapcsolat diagramm - Online szakácskönyv

A szakácskönyvben el kell tárolni az ételek nevét, elkészítésének szöveges leírását, az elkészítési időt, fényképet, valamint a hozzávalókat. A szakácskönyv olyan szempontból interaktív, hogy regisztrált felhasználók is tölthetnek fel receptet. Rajzolja le a szakácskönyv E-K diagramját! 

> #### _kódmágia_