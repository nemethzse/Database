> #### _kódmágia_

# Relációsséma - Nemzetközi táncverseny

Vegyük az elkészített E-K diagrammunkat:

![Megoldás](gifs/tancverseny.JPG)

Készítsük el a diagram alapján a relációssémánkat!

Megoldás:
```
TÁNC(tánc neve)
ZENE(cím, műfaj, előadó)
CSOPORT(nemzetiség, név, pontszám, átlag életkor)

TÁNCOL(nemzetiség, tánc neve, cím)
```
> #### _kódmágia_