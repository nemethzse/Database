> #### _kódmágia_

# Adatbázis készítés - Betegnyilvantartó

Vegyük az elkészített relációsémánkat:
```
BETEG(taj, név, lakcím)
GYÓGYSZER(gyógyszer neve)
BETEGSÉG(betegség neve)
VIZSGÁLAT(vkód, dátum)

ÉRZÉKENY(taj, gyógyszer neve)
GYÓGYSZERT FELÍR(gyógyszer neve, betegség neve, vkód, dátum)
LÁTOGAT(taj, vkód, dátum)
DIAGNOSZTIZÁL(betegség neve, vkód, dátum)
```
Hozzuk létre a MySQL adatbázist és vigyünk fel 7 demo tetszés szerinti demo adatot.

> #### _kódmágia_