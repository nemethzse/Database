> #### _kódmágia_

# Relációséma- Online szakácskönyv

Vegyük az elkészített E-K diagrammunkat:

![Megoldás](gifs/szakacskonyv.JPG)

Készítsétek el a diagram alapján a relációsémánkat!

Megoldás:
```
FELHASZNÁLÓK(név, userID, jelszó)
ÉTEL(fénykép, elkészítés, név, elkészítési idő, userID)
HOZZÁVALÓ(hozzávaló neve)

KELL HOZZÁ(hozzávaló neve, étel.név, userID, mennyiség)
```

> #### _kódmágia_