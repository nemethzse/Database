> #### _kódmágia_

# Egyed-Kapcsolat diagramm - Nemzetközi táncverseny

Olyan relációs adatbázist szeretnénk létrehozni, amely a Kék Osztriga Nemzetközi
Táncversenyen résztvevő csoportok adatait tartalmazza. Tároljuk a csoport nevét, nemzetiségét,
a csoport átlagéletkorát és a verseny folyamán elért pontszámot. Emellett tároljuk a bemutatott
tánc nevét, valamint a zenére vonatkozó adatokat, azaz a zene műfaját, a szám címét és
előadóját. 

Feltételezzük, hogy: 
- egyféle táncot több csoport is táncolhat,
- egy csoport többtáncot is előadhat, 
- a versenyen nincs kikötés a táncra vonatkozóan, így bármilyen táncot be lehet mutatni, 
- a zenét egyértelműen azonosítja a címe, azaz nincs két azonos című szám, 
- egy csoportban csak azonos nemzetiségűek táncolnak A tánc műfaja, valamint a csoportok
nemzetisége csak meghatározott értékeket vehet fel.

> #### _kódmágia_
