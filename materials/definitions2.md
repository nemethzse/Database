> #### _kódmágia_

# SQL nyelv

```definition```, ```sql```, ```xampp```

Mint tanultuk a relációs adatbázis szabványos lekérdező nyelve.

Szintaxis:
- a kulcsszavak nem case-sensitive-ek
- változó nincs, csak a táblára és a tábla oszlopaira lehet hivatkozni
- Relációjelek: =, <=, >=, !=, <> 
- Logikai műveletek: AND, OR, NOT
- Specciális logikai kifejezések:
    - x IS NULL: igaz, ha az x mező értéke NULL
    - x BETWEEN a AND b: igaz, ha a ≤ x ≤ b
    - x IN halmaz: igaz, ha x megegyezik a megadott halmaz egy elemével. Pl.: város IN (Budapest,Vác,Győr)
    - x relációjel ALL halmaz: igaz, ha x a halmaz minden elemével a megadott relációban van. Példa: fizetés != ALL (81000, 136000, 118000)
    - x relációjel ANY halmaz: igaz, ha a halmaznak van olyan eleme, amellyel x a megadott relációban van. Példa: fizetés < ANY (81000, 136000, 118000) 
    x LIKE minta: igaz, ha az x karaktersorozat megfelel a megadott mintának. Ha a mintában "%" illetve "_" jel szerepel, az tetszőleges karaktersorozatot illetve tetszőleges karaktert jelent. Példa: lakcím LIKE '%Vár u.%' igaz minden olyan lakcímre, amelyben szerepel a "Vár u." részlet. 
    - NOT kulcsszó a fentiekhez hozzáfűzve azok ellentétét jeleni. Pl.: x IS NOT IN(érték1,érték2,érték3)

## 1. Relációsémák
### 1.1. Relációsémák létrehozása

Relációséma létrehozására a CREATE TABLE utasítás szolgál, amely egyben egy üres
táblát is létrehoz a sémához. Az attribútumok definiálása mellett a kulcsok és külső kulcsok
megadására is lehetőséget nyújt:
```
CREATE TABLE táblanév
 ( oszlopnév adattípus [feltétel],
 ... ...,
 oszlopnév adattípus [feltétel]
 [, táblaFeltételek]
 );
 ```
Az adattípusok (rendszerenként eltérők lehetnek):
- CHAR(n) n hosszúságú karaktersorozat
- VARCHAR(n) legfeljebb n hosszúságú karaktersorozat
- INTEGER egész szám (röviden INT)
- REAL valós (lebegőpontos) szám, másnéven FLOAT
- DECIMAL(n[,d]) n jegyű decimális szám, ebből d tizedesjegy
- DATE dátum (év, hó, nap)
- TIME idő (óra, perc, másodperc)

Az adattípushoz "DEFAULT érték" megadásával alapértelmezett érték definiálható. Ha
ilyet nem adunk meg, az alapértelmezett érték NULL.
Feltételek (egy adott oszlopra vonatkoznak):
- PRIMARY KEY: elsődleges kulcs (csak egy lehet)
- UNIQUE: kulcs (több is lehet)
- REFERENCES tábla(oszlop) [ON-feltételek]: külső kulcs
 
Hozzuk létre az
- Osztály (osztálykód, osztálynév, vezAdószám)
- Dolgozó (adószám, név, lakcím, osztálykód)

relációsémákat SQL-ben: 
```
CREATE TABLE Osztály
 ( osztálykód CHAR(3) PRIMARY KEY,
 osztálynév CHAR(20),
 vezAdószám DECIMAL(10)
 );
CREATE TABLE Dolgozó
 ( adószám DECIMAL(10) PRIMARY KEY,
 név CHAR(30),
 lakcím CHAR(40) DEFAULT 'ismeretlen',
 osztálykód CHAR(3) REFERENCES Osztály(osztálykód)
 ); 
 ```
 
### 1.2. Relációsémák törlése
Szükség esetén törölhetjük is a relációsémát. (Vigyázat ilyenkor a táblában tárolt adatok elvesznek!)
```
DROP TABLE táblanév; 
```
Példa:
```
DROP TABLE Dolgozó; 
```

### 1.3. Relációsémák módosítása
Amikor egy meglévő relációsémát szeretnénk módosítani, megváltoztatni.
```
ALTER TABLE táblanév 
   [ADD (újelem, ..., újelem)] 
   [MODIFY (módosítás, ..., módosítás)] 
   [DROP (oszlop, ..., oszlop)]
```
Példa:
 ```
ALTER TABLE Dolgozó ADD (szüldátum DATE); 
ALTER TABLE Dolgozó MODIFY (lakcím VARCHAR(60)); 
ALTER TABLE Osztály 
  MODIFY (vezAdószám REFERENCES Dolgozó(adószám)); 
 ```
## 2. Adattábla műveletek
Egy meglévő relációséma szerint lértehozott adatbázis tábláinak adatain/értékein végzett műveletek.
### 2.1. Adattábla aktualizálása - hozzáadás
A táblába új sor felvétele. Ha oszloplista nem  szerepel,  akkor  valamennyi  oszlop  értéket  kap  a  
CREATE  TABLE-ben  megadott  sorrendben.  Egyébként  csak  az  oszloplistában  megadott   mező
k kapnak értéket, a többi mező értéke NULL lesz. 
```
INSERT INTO táblanév [(oszloplista)] VALUES (értéklista); 
```
Példa:
```
INSERT INTO Dolgozó (név, adószám) 
    VALUES ('Tóth Aladár', 1111); 
INSERT INTO Dolgozó 
    VALUES (1111, 'Tóth Aladár', , '12');
 ```
### 2.2. Adattábla aktualizálása - módosítás/frissítés
Az  értékadás  minden  olyan  soron  végrehajtódik,  amely  eleget  tesz  a  WHERE  feltételnek.  Ha  WHERE  feltétel  nem  szerepel,  akkor  az  értékadás  az  összes  sorra   megtörténik. 
```
UPDATE táblanév 
    SET oszlop = kifejezés, ..., oszlop = kifejezés 
  [ WHERE feltétel ]; 
```
Példa:
```
UPDATE Dolgozó 
    SET lakcím = 'Szeged, Rózsa u. 5.' 
    WHERE név = 'Kovács József'; 
UPDATE Dolgozó 
    SET osztálykód = '003' 
    WHERE osztálykód = '012'; 
UPDATE Dolgozó SET osztálykód = NULL; 
```
### 2.3. Adattábla aktualizálása - törlés
Hatására  azok  a  sorok  törlődnek,  amelyek  eleget  tesznek  a  WHERE  feltételnek.  Ha  a  WHERE  feltételt  elhagyjuk,  akkor  az  összes  sor  törlődik  (de  a  séma  megmarad)
 ```
DELETE FROM táblanév 
  [ WHERE feltétel ]; 
 ```
Példa:
```
  DELETE FROM Dolgozó 
    WHERE név = 'Kovács József'; 
DELETE FROM Osztály;
```
### 2.4. Adattábla lekérdezése
Lekérdezésre  a  SELECT  utasítás  szolgál, amely  egy  vagy  több  adattáblából  egy eredménytáblát állít  elő.
```
SELECT  [DISTINCT]  oszloplista
    FROM  táblanévlista
   [WHERE  feltétel]
```
Példa:
```
SELECT * FROM Könyv WHERE kivétel < 2013.01.01;
```
### 2.5. Adattábla lekérdezésének rendezése
A  SELECT  utasítás  végére  helyezhető,  és  az  eredménytáblának  a  megadott  oszlopok szerinti rendezését írja elő. Az oszlopnév után írt ASC (ascending) növekvő, DESC (descending)  csökkenő sorrendben  való  rendezést  jelent.  
Alapértelmezés  szerint  a  rendezés növekvő sorrendben történik, ezért ASC kiírása fölösleges. 
ORDER BY oszlopnév [DESC], ..., oszlopnév [DESC] 
```
 SELECT osztkód, név, fizetés FROM Dolgozó 
  ORDER BY osztkód, fizetés DESC; 
```
Példa:
```
SELECT * FROM Könyv WHERE kivétel < 2013.01.01 ORDER BY szerző DESC;
```
## 3. XAMPP 

Egy nyílt forráskódú programcsomag, amit főleg webes fejlesztésekhez használnak. Ezek közül ami nekünk most fontos, az a mySQL. Ami egy A MySQL egy többfelhasználós, többszálú, SQL-alapú relációs adatbázis-kezelő szerver.

[Hivatalos oldal](https://www.apachefriends.org/hu/index.html)

Előfordulhat, hogy meglévő programjaink valamelyikével portütközés lép fel, ekkor az alapértelmezett portokat át kell állítsuk.
[Segítség](https://stackoverflow.com/questions/11294812/how-to-change-xampp-apache-server-port)


> #### _kódmágia_
