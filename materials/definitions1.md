> #### _kódmágia_

# Relációs adatbázisok

```definition```, ```relációs adatbázis```, ```E-K modell```, ```adatbázis séma```

Adatbázison köznapi értelemben adatok valamely célszerűen rendezett, valamilyen szisztéma szerinti tárolását értjük. Az adatbázis esetén nem az adatok nagy számán van a hangsúly, hanem a célszerű rendezettségen.

Adatbáziskezelő rendszer feladatai:
- adatstruktúra leírása
- lekérdezési lehetőségek
- adatok aktualizálása (új felvitel,törlés, módosítás)
- adatbiztonsági mechanizmusok (rendelkezésre állás, sértetlenség, bizalmasság)

## 1. Relációs adatbázisok

A relációs modellben az adatokat 2D táblázatok soraiban képezzük le. Itt nincsenek előre definiált kapcsolatok az egyes adategységek között, hanem a kapcsolatok létrehozásához szükséges adatokat tároljuk többszörösen. 

Lekérdező nyelve az SQL (Structured Query Language).

Néhány relációs adatbázis-kezelő
- Oracle
- MySQL
- PostgreSQL
- SQLite

## 2. Egyed-Kapcsolat modell

Grafikus leíró eszköz, diagram segítségével szemléletesen adja meg az adatbázis
struktúráját. (E-K modell, Entity-Relationship Model, E-R model)

Tegyük fel, hogy egy könyvtár kölcsönzési nyilvántartását szeretnénk
adatbázissal megoldani. Ehhez nyilvántartást kell vezetni
- a könyvekről,
- az olvasókról,
- a kikölcsönzési és visszahozási időpontokról.
A modell megalkotásához néhány alapfogalmat meg kell ismernünk. 

Egyed (Entitás) - a valós világban létező dolgot, amit tulajdonságokkal akarunk leírni. Pl.: Könyv
Tulajdonság (attribútum) - egyed egy jellemzője. Pl: szerző
Kulcs - minimális attribútumhalmaz, amely egyértelműen meghatározza az egyedet - könyvszám
Kapcsolat - két logikailag összetartozó egyed között áll fenn. Pl.: kölcsönzés

EK diagramm: 
egyed - téglalap
attribútum - ellipszis
kapcsolat - rombusz

![EK példa](gifs/ek_konyvtar.jpg)

Kapcsolat típusai:
- 1:1 (egy az egyhez)
- 1:N (egy a többhöz)
- N:M (több a többhöz)

[Lásd](gifs/ek_kapcsolat_tipusai.png)

Összetett attribútum: maga is attribútumokkal rendelkezik
Többértékű attribútum: aktuális értéke halmaz vagy lista lehet. Pl.: könyv szerzői ha nem fontos a sorrend


## 3. E-K diagramból relációs adatbázisséma készítése

Adatbázis séma: az adatbázis szerkezete (milyen egyedeket tartalmaz, milyen attribútumokkal)

- Egyedek leképezése: : az E-K modell minden egyedéhez felírunk egy relációsémát, amelynek neve az egyed neve, attribútumai az egyed attribútumai, elsődleges kulcsa az egyed kulcsattribútuma(i)
 Pl.: Könyv (könyvszám, szerző, cím) 
- Gyenge entitások leképezése:  a felírt relációsémát bővíteni kell a meghatározó  kapcsolatokban szereplő egyedek kulcsával
Pl.: egy számítógépszervíz nem rendel egyedi azonosítót a számítógépekhez, hanem tulajdonosaik szerint tartja nyilván. Így fel kell venni a tulajdonos egyedi azonosítóját is.
Számítógép (processor, memória, merevlemez, személyiszám)
- Összetett attribútumok leképezése
Pl.: Olvasó (olvasószám, név, lakcím) -› Olvasó (olvasószám, név, irszám, város, utca)
- Többértékű attribútumok leképezése
Pl.: Új tábla felvéte - Könyv(könyvszám, cím, szerző ) -› Könyv(könyvszám, cím) Szerző(könyvszám, Szerző)
- Kapcsolatok leképezése: Vegyünk fel egy új sémát, melynek neve a kapcsolat neve, attribútumai pedig a kapcsolódó entitások kulcs attribútumai és a kapcsolat saját attribútumai
Pl.: Kölcsönöz(könyvszám,olvasószám,kivétel,visszahozás)

## 4. Relációs adatbázis normalizálása

Nézzük az alábbi példát, egy dolgozói nyilvántartást:

Dolgozó (név, adószám, cím, osztálykód, osztálynév, vezAdószám)
```
Név Adószám Cím Osztálykód Osztálynév VezAdószám
Kovács 1111 Pécs, Vár u.5. 2 Tervezési 8888
Tóth 2222 Tata, Tó u.2. 1 Munkaügyi 3333
Kovács 3333 Vác, Róka u.1. 1 Munkaügyi 3333
Török 8888 Pécs, Sas u.8. 2 Tervezési 8888
Kiss 4444 Pápa, Kő tér 2. 3 Kutatási 4444
Takács 5555 Győr, Pap u. 7. 1 Munkaügyi 3333
Fekete 6666 Pécs, Hegy u.5. 3 Kutatási 4444
Nagy 7777 Pécs, Cső u.25. 3 Kutatási 4444 
```
Nem helyes, mert 
- redundancia keletkezhet 
- bizonyos adatok modosítás esetén több más helyen kell módosítani
- új felvétel esetén előfordulhat hibás adat (kulcs ugyanaz de a név más)
- törlés esetén fontos adatok elveszhetnek

Kerüljük a redundanciát, meg kell szüntetni!

### 4.1 Normál formák alkalmazása

- egy relációséma csak egyszerű, atomi adatokból áll
- egy relációsémában ha minden másodlagos attribútum teljesen függ bármely kulcstól. Vagyis egy attribútum több kulcs esetén mindegyiktől függ.

Pl.: Tegyük fel, hogy egy vállalat dolgozói különféle projekteken dolgoznak
meghatározott heti óraszámban. 

```
Adószám Név Projektkód Óra Projektnév Projekthely Lakcím
 1111 Kovács P2 4 Adatmodell Veszprém 1068, Budapest Hős utca 6
 2222 Tóth P1 6 Hardware Budapest 1068, Budapest Hős utca 6
 4444 Kiss P1 5 Hardware Budapest 1068, Budapest Hős utca 6
 1111 Kovács P1 2 Hardware Budapest 1068, Budapest Hős utca 6
 1111 Kovács P5 8 Teszt Szeged 1068, Budapest Hős utca 6
 8888 Török P2 12 Adatmodell Veszprém 1068, Budapest Hős utca 6
 5555 Takács P5 3 Teszt Szeged 1068, Budapest Hős utca 6
 6666 Fekete P5 4 Teszt Szeged 1068, Budapest Hős utca 6
 8888 Török P3 4 Software Veszprém 1068, Budapest Hős utca 6
 7777 Nagy P3 14 Software Veszprém 1068, Budapest Hős utca 6
```
Összetett attribútum:
- lakcím

Függőségek:
- adószám → név
- projektkód → {projektnév, projekthely}
- {adószám, projektkód} → óra

Megoldás:
```
Dolgozó (adószám, név, irszám, város, utca, házszám)
Projekt (projektkód, projektnév, projekthely)
Dolgozott (adószám, projektkód, óra) 
```